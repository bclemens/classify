# classify
### classification banner for bspwm
the classifcation banner utility provided by redhat (courtesy of frank caviggia), though excellent in gnome, does not play well with tiling window managers.  this is a simple script using lemonbar-xft that configures bspwm itself and cleans up when you close it.

# usage
classify -c [keyword] -d [top offset in pixels] -y [banner height in pixels]
-d changes the top banners postion relevant to the top of the screen, useful if you have a top panel
click the banner text to exit

| keyword | classification             | text                                  | colours         |
| ------- | -------------------------- | ------------------------------------- | --------------- |
| u       | unclassified               | UNCLASSIFIED                          | white on green  |
| fouo    | unclassified//fouo         | UNCLASSIFIED // FOR OFFICIAL USE ONLY | white on green  |
| c       | confidential               | CONFIDENTIAL                          | white on blue   |
| s       | secret                     | SECRET                                | white on red    |
| ts      | top secret                 | TOP SECRET                            | white on orange |
| sci     | top secret//sci            | TOP SECRET // SCI                     | black on yellow |
| tk      | top secret//si//tk         | TOP SECRET // SI/TK                   | black on yellow |
| core    | top secret//comint//noforn | TOP SECRET // COMINT // NOFORN        | black on yellow |
_noforn isn't added to tk by default to avoid overclassification, you may want to add it_

![tk sample](assets/tk.png)
![fouo sample](assets/fouo.png)

# todo:
+ add bottom banner vertical offset option, even though bottom panels are lame
